#Dhafiano Fadeyka Ghani Wiweko
#2106751631
#DDP 1
#LAB 3


import random

#Membuat user input
masuk_inp = input("Masukkan pesan yang akan dikirim: ")
masuk_inp = masuk_inp.lower()
isi_nilai = input("Pilih nilai n: ")
isi_nilai = int(isi_nilai)
isi_enc = ""

#Susunan kode untuk membuat pesan rahasia
for a in range(len(masuk_inp)):
    if masuk_inp[a].isnumeric():
        kata = (ord(masuk_inp[a]) + isi_nilai - 48) % 10 + 48
        kata = (kata + isi_nilai - 48) % 10 + 48
        isi_enc += chr(kata)
    elif masuk_inp[a] == ' ':
        isi_enc += masuk_inp[a]
    else:
        kata = (ord(masuk_inp[a]) + isi_nilai - 97) % 26 + 97
        isi_enc += chr(kata)

enc_baris = ""
a = 0

#Membaca pesanrahasia dan membuat rumusannya
with open("pesanrahasia.txt") as b:
    cap = 0
    baris_kata = b.read()
    a_key = random.randint(100, len(baris_kata)-100)
    a_key2 = 20220310 - isi_nilai
    akhir_key = '*' + str(a_key2) + '*'
    while a < len(baris_kata):
        if cap < len(isi_enc):
            if isi_enc[cap].isnumeric():
                enc_baris += isi_enc[cap]
                cap += 1
                a -= 1
            elif isi_enc[cap] == " ":
                enc_baris += '$'
                cap += 1
                a -= 1
            else:
                if isi_enc[cap] == baris_kata[a]:
                    enc_baris += baris_kata[a].upper()
                    cap += 1
                else:
                    enc_baris += baris_kata[a]
        else:
            enc_baris += baris_kata[a]
        a += 1
enc_baris = enc_baris[:a_key] + \
    akhir_key + enc_baris[a_key:]
print(enc_baris)
