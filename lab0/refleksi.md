1. Apa hal baru yang kamu pelajari melalui lab ini?
2. Adakah kesulitan yang kamu alami saat mencoba git? Jelaskan.
3. Hal apa yang masih sulit kamu pahami?

Jawaban:

1. Hal yang baru saya pelajari pada Lab 0 ini adalah cara penggunaan git.
2. Sulitnya ketika pada tahap git push origin master dimana saya selalu terkena authentication failed terus-menerus.
3. Sepertinya sudah tidak ada.