harga_pulpen = 10000
harga_pensil = 5000
harga_cortape = 15000

print("Toko ini menjual: ")
print("1. pulpen " + str(harga_pulpen) + "/pcs")
print("2. pensil " + str(harga_pensil) + "/pcs")
print("3. correction tape " + str(harga_cortape) + "/pcs")

print("Masukkan jumlah barang yang ingin anda beli")
jml_pulpen = int(input("Pulpen: "))
jml_pensil = int(input("Pensil: "))
jml_cortape = int(input("Correction tape: "))

print("Ringkasan pembelian")
print(f"{jml_pulpen} pulpen x {harga_pulpen}")
print(f"{jml_pensil} pensil x {harga_pensil}")
print(f"{jml_cortape} cortape x {harga_cortape}")

total_harga1 = jml_pulpen * harga_pulpen
total_harga2 = jml_pensil * harga_pensil
total_harga3 = jml_cortape * harga_cortape

total_harga_semua = total_harga1 + total_harga2 + total_harga3
print(f"Total harga: {total_harga_semua}")