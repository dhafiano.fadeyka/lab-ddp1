#Dhafiano Fadeyka Ghani Wiweko
#2106751631
#Lab 5

# Membuat pembukaa matrix calculator
printan = """Selamat datang di Matrix Calculator. Berikut adalah operasi-operasi yang
dapat dilakukan:
1. Penjumlahan
2. Pengurangan
3. Transpose
4. Determinan
5. Keluar"""

# Game dimulai
while True:
    print(printan)
    ret = input("Silakan pilih operasi: ")
    # Untuk matriks penjumlahan
    if ret == "1":
        size = input("Ukuran matriks: ")
        first_mat =[]
        if size[0].isdigit():
            for i in range(1, int(size[0])+1):
                list = []
                bar_inpx = input(f"Baris {i} matriks 1: ")
                list = bar_inpx.split()
                list = [int(i) for i in list]
                first_mat += [list]

        second_mat =[]
        if size[2].isdigit():
            for a in range(1, int(size[2])+1):
                bar_inpy = input(f"Baris {a} matriks 2: ")
                list = []
                bar_inpx = input(f"Baris {a} matriks 1: ")
                list = input_baris1.split()
                list = [int(a) for a in list]
                second_mat += [list]

        i = numpy.array(first_mat)
        j = numpy.array(second_mat)
        print (numpy.add(i,j))
    # Untuk matrix pengurangan
    if ret == "2":
        size = input("Ukuran matriks: ")
        first_mat =[]
        if size[0].isdigit():
            for i in range(1, int(size[0])+1):
                list = []
                bar_inpx = input(f"Baris {i} matriks 1: ")
                list = bar_inpx.split()
                list = [int(i) for i in list]
                first_mat += [list]

        second_mat =[]
        if size[2].isdigit():
            for a in range(1, int(size[2])+1):
                bar_inpy = input(f"Baris {a} matriks 2: ")
                list = []
                bar_inpx = input(f"Baris {a} matriks 1: ")
                list = input_baris1.split()
                list = [int(a) for a in list]
                second_mat += [list]

        i = numpy.array(first_mat)
        j = numpy.array(second_mat)
        print (numpy.add(i,j))
    # Untuk matriks transpose
    if ret == "3":
        size = input("Ukuran matriks: ")
        first_mat =[]
        if size[0].isdigit():
            for i in range(1, int(size[0])+1):
                list = []
                bar_inpx = input(f"Baris {i} matriks 1: ")
                list = bar_inpx.split()
                list = [int(i) for i in list]
                first_mat += [list]

        second_mat =[]
        if size[2].isdigit():
            for a in range(1, int(size[2])+1):
                bar_inpy = input(f"Baris {a} matriks 2: ")
                list = []
                bar_inpx = input(f"Baris {a} matriks 1: ")
                list = input_baris1.split()
                list = [int(a) for a in list]
                second_mat += [list]

        i = numpy.array(first_mat)
        j = numpy.array(second_mat)
        print (numpy.add(i,j))
    # Untuk matriks determinan
    if ret == "4":
        size = input("Ukuran matriks: ")
        first_mat =[]
        if size[0].isdigit():
            for i in range(1, int(size[0])+1):
                list = []
                bar_inpx = input(f"Baris {i} matriks 1: ")
                list = bar_inpx.split()
                list = [int(i) for i in list]
                first_mat += [list]

        second_mat =[]
        if size[2].isdigit():
            for a in range(1, int(size[2])+1):
                bar_inpy = input(f"Baris {a} matriks 2: ")
                list = []
                bar_inpx = input(f"Baris {a} matriks 1: ")
                list = input_baris1.split()
                list = [int(a) for a in list]
                second_mat += [list]

        i = numpy.array(first_mat)
        j = numpy.array(second_mat)
        print (numpy.add(i,j))
    #Untuk mengakhiri game
    if ret == "5":
        print("Sampai jumpa")