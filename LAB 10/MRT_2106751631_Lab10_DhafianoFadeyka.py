# Dhafiano Fadeyka Ghani Wiweko
# 2106751631
# DDP 1
# Lab 10

# Membuka tkinter
from tkinter import *
from tkinter.filedialog import asksaveasfilename, askopenfilename

# Pembuatan aplikasinya
class Application(Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.initUI()
        self.create_buttons()
        self.create_editor()
    # Pembuatan lebar tinggi aplikasinya
    def initUI(self):
        self.master.title("Pacil Editor")
        self.master.geometry("500x500")
        self.pack(fill=BOTH, expand=True)

        self.frameButtons = Frame(self)
        self.frameButtons.pack(fill=X, side=TOP)
        pass
    # Pembuatan buttonnya
    def create_buttons(self):
        self.btnOpenFile = Button(
            self.frameButtons, text="Open File", command=self.load_file)
        self.btnOpenFile.pack(side=LEFT, padx=5, pady=5)

        self.btnSaveFile = Button(
            self.frameButtons, text="Save File", command=self.save_file)
        self.btnSaveFile.pack(side=LEFT, padx=5, pady=5)

        self.btnCloseProgram = Button(
            self.frameButtons, text="Quit Program", command=self.master.quit)
        self.btnCloseProgram.pack(side=LEFT, padx=5, pady=5)
        pass
    # Membuat kotak text editornya
    def create_editor(self):
        self.textboxEditor = Text(self)
        self.textboxEditor.pack(fill=X, side=TOP, padx=5, pady=5)
        pass
    # load filenya
    def load_file_event(self, event):
        self.load_file()

    def load_file(self):
        file_name = askopenfilename(
            filetypes=[("All files", "*")]
        )
        if not file_name:  # Jika pengguna membatalkan dialog, langsung return
            return
        text_file = open(file_name, 'r', encoding="utf-8")
        result = text_file.read()
        text_file.close()
        self.set_text(text=result)
    # Tempat menyimpan file
    def save_file_event(self, event):
        self.save_file()
    # Function pada menyimpan filenya
    def save_file(self):
        file_name = asksaveasfilename(
            filetypes=[("All files", "*")]
        )
        if not file_name:  # Jika pengguna membatalkan dialog, langsung return
            return

        text = self.get_text()
        text_file = open(file_name, 'w', encoding="utf-8")
        text_file.write(text)
        text_file.close()
    # Penyusunan textnya
    def set_text(self, text=''):
        self.textboxEditor.delete('1.0', END)
        self.textboxEditor.insert('1.0', text)
        self.textboxEditor.mark_set(INSERT, '1.0')
        self.textboxEditor.focus()
    # Function mendapatkan textnya
    def get_text(self):
        return self.textboxEditor.get('1.0', END+'-1c')


if __name__ == "__main__":
    root = Tk()
    app = Application(master=root)
    app.mainloop()

# Berkolaborasi dengan Akmal